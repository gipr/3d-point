#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm> 
#include <map>

using namespace std;
struct Point
{
    double x_, y_, z_;
    Point(double x, double y, double z) : x_(x), y_(y), z_(z) {}
};

struct PointWithDist
{
    Point P_;
    double D_;
    PointWithDist(Point p, double d) : P_(p), D_(d) {}
};

bool distComp (PointWithDist i,PointWithDist j) { return (i.D_<j.D_); }

class PointUtil
{

  public:
    vector<PointWithDist> result;

    double distance_between(Point p1, Point p2)
    {

        return hypot(hypot(p1.x_ - p2.x_, p1.y_ - p2.y_), p1.z_ - p2.z_);
    }
    
 
  


    void filterByRadius(Point centre, int R, vector<Point> pts_)
    {
       

        for (int i = 0; i < pts_.size(); i++)
        {
            double d = distance_between(centre, pts_[i]);
            if (d <= R)
            {
                PointWithDist pwd(pts_[i], d);
                result.push_back(pwd);
            }
        }

       sort(result.begin(),result.end(),distComp);
    }

    

    
};

void printpoint(PointWithDist p)
{
    cout << "(" << p.P_.x_ << "," << p.P_.y_ << "," << p.P_.z_ << ")" <<"----"<<p.D_<< endl;
    
}

int main()
{

    vector<Point> input_vec;
    vector<PointWithDist> output_vec;

    Point a(1.1, 1.2, 1.2);
    Point b(2, 2, 6);
    Point c(9.3, 4.8, 2.12);
    Point d(2, 2, 3);
    Point e(4, 5, 6);

    input_vec.push_back(e);
    input_vec.push_back(b);
    input_vec.push_back(c);
    input_vec.push_back(d);
    input_vec.push_back(a);

    PointUtil pu;
    Point centre(1, 1, 1);

    pu.filterByRadius(centre, 5, input_vec);
    
    for (int i = 0; i < pu.result.size(); i++)
    {
        printpoint(pu.result[i]);
    }
}